# bower

bower install したファイルからデプロイするものだけを抜き出してみる例。
grunt-bower-task, grunt-preen の2つを使ってみた。


1 つの js にまとめないのは、RequireJS で CDN に js がなかったら、
webサーバに置いているやつを読む。
という事を想定しているから。


まとめるのと比べて良いのかはよく分からない。
その js がキャッシュされているか? で速度が変わるので、
状況による気もする。


## grunt-bower-task

grunt-bower-task は、bower_components から使うファイルを指定したディレクトリにコピーする様になっている。
bower.json の main の部分がデフォルトで対象になる。
でも、main に出てくるものは、CDN に置いてある様な minify されたものじゃなくて、
他の物と混ぜて minify する為の物らしい。


なので、bower.json に exportsOverride を追加して、
どうコピーするか指定する必用がある。
開発時のみに使うなら問題ないけど、
いちいち書くのは面倒だと思う。
もしかすると RequireJS みたいなものを使うならいいのかもしれない。

    "*": {
      "js": "dist/*.min.js"
    }
    
とか書いたら、ちょっと楽できるかなと思ったけど、
css, fonts とか js じゃないものが出なくなってた。


あと、ディレクトリの指定が Gruntfile.js と bower.json に分かれているのが微妙な気がした。

## grunt-preen

grunt-preen は、bower_components のファイルからいらないものを消す様になっている。
コピーしないで、あるものをそのまま使うというのは、記憶する事を減らすメリットに繋がると思う。


## grunt-wiredep

これは試していない


HTMLに

    <!-- bower:js -->
    <!-- endbower -->
    
とか書いたら

    <!-- bower:js -->
    <script src="bower_components/jquery/jquery.js"></script>
    <!-- endbower -->
    
という風に書換えてくれるみたい。

## minify する場合

独自部分も含めて r.js で minify するなら
ディレクトリに分けたりしないで
(main-bower-files)[https://github.com/ck86/main-bower-files]
使って、ビルドするときに扱える様にすればいいんじゃないかと思う。
やってないので、出来るか分からないけど。


この場合は、bower_components のファイルをわざわざ、コピーしたり消したりする必用はないと思う。


## その他

https://bower.io/docs/tools/
で、いくつか周辺ツールが紹介したあった。
