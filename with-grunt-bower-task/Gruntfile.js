module.exports = function (grunt) {
    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: './docroot/components',
                    layout: 'byType', // or 'byComponent'
                    install: true,
                    verbose: false,
                    cleanTargetDir: true,
                    cleanBowerDir: false
                }}}});
    grunt.loadNpmTasks('grunt-bower-task');
};
