#!/bin/sh

if [ "$1" = "clean" ] ; then
  rm -rf node_modules bower_components docroot/components
  exit
fi


# INSTALL these package globally if not installed.
#   npm install bower grunt-cli -g


# INSTALL the bower packages:
bower install
# The directory where bower packages will be installed is described in .bowerrc.
#
# If the bower.json isn't exists, run:
#    bower install .... --save
#    bower install .... --save-dev


# INSTALL these locally:
npm install grunt grunt-bower-task --save-dev


# COPY bower package files to `targetDir` of the Grantfile.
grunt bower:install


cat<<EOF
DONE!

Browse docroot/index.html
EOF
