#!/bin/sh

if [ "$1" = "clean" ] ; then
  rm -rf node_modules docroot/components
  exit
fi

# 設定ファイルとか全部小さいので、ヒアドキュメントで作る様にしてもいいなと思う


# INSTALL these package globally if not installed.
#   npm install bower grunt-cli -g


# INSTALL the bower packages:
bower install
# The directory where bower packages will be installed is described in .bowerrc.
#
# If the bower.json isn't exists, run:
#    bower install .... --save
#    bower install .... --save-dev


# INSTALL these locally:
npm install grunt grunt-preen --save-dev


# REMOVE bower package files will not be used.
# RUN one of these:
#   grunt preen:preview
grunt preen:verbose
#   grunt preen
#
# The files will remain(not be removed) is described by bower packages in `preen` property of bower.json.


cat<<EOF
DONE!

Browse docroot/index.html
EOF
